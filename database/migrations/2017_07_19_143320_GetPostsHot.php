<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GetPostsHot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		$sql = "DROP PROCEDURE IF EXISTS GetPostsHot;
				CREATE PROCEDURE GetPostsHot(IN `tag_id_param` BIGINT)
				BEGIN
					IF tag_id_param = -1 THEN
						SELECT id, title, body, tag_id, created_at, updated_at, post_type, score/POWER(HOUR(TIMEDIFF(UTC_TIMESTAMP(), created_at))+2 , 1.8) as score
						FROM posts
						WHERE DATEDIFF(UTC_TIMESTAMP(), created_at) <= 7 AND is_deleted = 0
						ORDER BY score DESC;
					ELSE
						SELECT id, title, body, tag_id, created_at, updated_at, post_type, score/POWER(HOUR(TIMEDIFF(UTC_TIMESTAMP(), created_at))+2 , 1.8) as score
						FROM posts
						WHERE DATEDIFF(UTC_TIMESTAMP(), created_at) <= 7 AND tag_id = tag_id_param AND is_deleted = 0 
						ORDER BY score DESC;
					END IF;
				END";
		DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
