<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_posts', function (Blueprint $table) {
            $table->increments('id');
			$table->bigInteger('post_id');
			/*
				Undocumented magical constants yay!
				DeleteType:
					1: Spam
					2: Personal Info
					3: Other
			*/
			$table->integer('delete_type');
			$table->string('delete_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_posts');
    }
}
