<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_comments', function (Blueprint $table) {
            $table->increments('id');
			$table->bigInteger('comment_id');
			/*
				Undocumented magical constants yay!
				DeleteType:
					1: Spam
					2: Personal Info
					3: Other
					4: Parent post deleted
			*/
			$table->integer('delete_type');
			$table->string('delete_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_comments');
    }
}
