<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_post extends Model
{
	public function post() {
      return $this->belongsTo(Post::class, 'post_id');
    }
}
