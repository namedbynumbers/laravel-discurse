<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function tag() {
      return $this->belongsTo(Tag::class, 'tag_id');
    }

	public function comments() {
		return $this->hasMany(Comment::class, 'post_id');
	}

	public function upvotes() {
		return $this->hasMany(Post_upvote::class, 'post_id');
	}

	protected $fillable = array('id', 'title', 'body', 'score', 'tag_id', 'created_at', 'updated_at', 'ip_address', 'post_type');
}
