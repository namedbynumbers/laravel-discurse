<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_comment extends Model
{
	public function comment() {
      return $this->belongsTo(Comment::class, 'comment_id');
    }
}
