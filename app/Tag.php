<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function posts() {
		return $this->hasMany(Post::class, 'tag_id');
    }

	public static function getTagIdByName($name) {
		$tags = static::where('name', '=', $name)->get();
		if(count($tags) > 0) {
			return $tags[0]->id;
		} else {
			return -2;
		}
	}
}
