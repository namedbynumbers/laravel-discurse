<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Tag;
use App\Comment;
use App\User;
use App\Banlist;
use App\Deleted_comment;
use App\Deleted_post;

class AdminController extends Controller
{
    /*
	get -- get the admin panel page
	*/
	public function get() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1 || $user->type == 2)) {
			$posts = Post::where('report_count', '>=', 1)->where('is_deleted', '=', 0)->orderBy('report_count', 'desc')->get();
			$comments = Comment::where('report_count', '>=', 1)->where('is_deleted', '=', 0)->orderBy('report_count', 'desc')->get();
			$moderators = User::where('type', '=', 2)->get();
			$banlist = Banlist::all();
			return view('admin/mod', ['posts' => $posts, 'comments' => $comments, 'moderators' => $moderators, 'banlist' => $banlist]);
		} else {
			return redirect('/');
		}
	}
	/*
	deletePost -- delete a post by Id.
	*/
	public function deletePost() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1 || $user->type == 2)) {
			$postsToDelete = request('postsToDelete');
			$postDeleteType = request('postDeleteType');
			$postDeleteComment = request('postDeleteComment');

			foreach($postsToDelete as $postId) {
				$post = Post::find($postId);
				//delete all the comments belonging to the post.
				if(!is_null($post)) {
					$comments = $post->comments;
					foreach($comments as $comment) {
						if($comment->is_deleted === 0) {
							$comment->is_deleted = 1;
							$comment->save();
							$commentDeleted = new Deleted_comment();
							$commentDeleted->comment_id = $comment->id;
							$commentDeleted->delete_type = 4;
							$commentDeleted->delete_reason = $postDeleteComment;
							$commentDeleted->save();
						}
					}
					$post->is_deleted = 1;
					$post->save();

					$postDeleted = new Deleted_post();
					$postDeleted->post_id = $post->id;
					$postDeleted->delete_type = $postDeleteType;
					$postDeleted->delete_reason = $postDeleteComment;
					$postDeleted->save();
				}
			}
			return json_encode(true);
		}
		return json_encode(false);
	}
	/*
	deleteComment -- delete a comment by Id.
	*/
	public function deleteComment() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1 || $user->type == 2)) {
			$commentsToDelete = request('commentsToDelete');
			$commentDeleteType = request('commentDeleteType');
			$commentDeleteComment = request('commentDeleteComment');

			foreach($commentsToDelete as $commentId) {
				$comment = Comment::find($commentId);
				//delete all the comments belonging to the post.
				if(!is_null($comment) && $comment->is_deleted === 0) {
					$comment->is_deleted = 1;
					$comment->save();

					$commentDeleted = new Deleted_comment();
					$commentDeleted->comment_id = $comment->id;
					$commentDeleted->delete_type = $commentDeleteType;
					$commentDeleted->delete_reason = $commentDeleteComment;
					$commentDeleted->save();
				}
			}
			return json_encode(true);
		}
		return json_encode(false);
	}

	/*
	addModerator -- add a moderator. Boy, these comments sure are super helpful.
	*/
	public function addModerator() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1)) {
			$username = request('username');
			if(!is_null($username)) {
				$moderator = User::where('username', '=', $username)->first();
				if(!is_null($moderator) && $moderator->type != 1) {
					$moderator->type = 2;
					$moderator->save();
					return json_encode(true);
				}
			}
		}
		//if any condition is not satisfied.
		return json_encode(false);
	}

	/*
	deleteModerator -- delete a moderator
	*/
	public function deleteModerator() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1)) {
			$userId = request('userId');
			if(!is_null($userId)) {
				$moderator = User::find($userId);
				if(!is_null($moderator) && $moderator->type == 2) {
					$moderator->type = 3;
					$moderator->save();
					return json_encode(true);
				}
			}
		}
		//if any condition is not satisfied.
		return json_encode(false);
	}

	/*
	addBannedIp -- ban an ip
	*/
	public function addBannedIp() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1)) {
			$address = request('address');
			$banned = Banlist::where('ip_address', '=', $address)->first();
			if(is_null($banned)) {
				$newBannedIp = new Banlist();
				$newBannedIp->ip_address = $address;
				$newBannedIp->banned_by = $user->id;
				$newBannedIp->save();
				return json_encode(true);
			}
		}
		return json_encode(false);
	}

	/*
	deleteBannedIp -- remove a banned ip
	*/
	public function deleteBannedIp() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1)) {
			$addressId = request('addressId');
			$banned = Banlist::find($addressId);
			if(!is_null($banned)) {
				$banned->delete();
				return json_encode(true);
			}
		}
		return json_encode(false);
	}

	/*
	getModLogs -- gets a list of deleted comments and posts.
	*/
	public function getModLogs() {
		$deletedPosts = Deleted_post::orderBy('created_at', 'desc')->get();
		$deletedComments = Deleted_comment::where('delete_type', '<>', 4)->orderBy('created_at', 'desc')->get();
		return view('admin/modlogs', ['deletedPosts' => $deletedPosts, 'deletedComments' => $deletedComments]);
	}

	/*
	addTag
	*/
	public function addTag() {
		$user = auth()->user();
		if(!is_null($user) && ($user->type == 1)) {
			$tagText = request('tagText');
			$tag = Tag::where('name', '=', $tagText)->first();
			if(is_null($tag)) {
				$newTag = new Tag();
				$newTag->name = $tagText;
				$newTag->save();
				return json_encode(true);
			}
		}
		return json_encode(false);
	}

}
