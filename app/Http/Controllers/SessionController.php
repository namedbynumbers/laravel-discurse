<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banlist;

class SessionController extends Controller
{

	public function store() {
		$ipAddress = request()->ip();
		$banned = Banlist::where('ip_address', '=', $ipAddress)->first();
		if(is_null($banned)) {
			if(!auth()->check()) {
		        if(!auth()->attempt(request(['username', 'password']))) {
		            return back()->withErrors(
		                ['message' => 'Username and password do not match']
		            );
		        }
			}
		}
        return redirect('/');
    }

	public function destroy() {
		if(auth()->check()) {
        	auth()->logout();
		}
        return redirect('/');
    }
}
