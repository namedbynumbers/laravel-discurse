<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Post;
use App\Tag;
use App\Comment;
use App\Banlist;
use GrahamCampbell\Markdown\Facades\Markdown;

class PostController extends Controller
{
	/*
	get() -- fetch all the posts for the front page
	*/
    public function get() {
		$page = (int) request()->input('p');
		if($page <= 0) {
			$page = 1;
		}

		//maximum number of posts that can be shown on a page.
		$maxPosts = 20;

		$postsArray = \DB::select('CALL GetPostsHot(?)', array(-1));
		$posts = [];

		foreach($postsArray as $p) {
			array_push($posts, new Post((array) $p));
		}

		$posts = collect($posts);
		$posts = $posts->forPage($page, $maxPosts);

		if(auth()->check()) {
			$userUpvotes = auth()->user()->postUpvotes;
			foreach($posts as $post) {
				$post->isUpvoted = false;
				if($userUpvotes->contains('post_id', $post->id)) {
					$post->isUpvoted = true;
				}
			}
		}
		return view('posts/index', ['posts' => $posts, 'nextPage' => $page+1]);
    }

	/*
	getNew() -- fetch all the new posts for the front page sorted by time
	*/
	public function getNew() {
		$page = (int) request()->input('p');
		if($page <= 0) {
			$page = 1;
		}
		//maximum number of posts that can be shown on a page.
		$maxPosts = 20;

		$posts = Post::whereDate('created_at', '>=', Carbon::now()->subDays(7))->get()->sortByDesc('created_at');

		$posts = $posts->forPage($page, $maxPosts);

		if(auth()->check()) {
			$userUpvotes = auth()->user()->postUpvotes;
			foreach($posts as $post) {
				$post->isUpvoted = false;
				if($userUpvotes->contains('post_id', $post->id)) {
					$post->isUpvoted = true;
				}
			}
		}
		return view('posts/index', ['posts' => $posts, 'nextPage' => $page+1]);
	}

	/*
	getByTag -- fetch all the posts for a particular tag
	TODO: Merge it with the get() function
	*/
	public function getByTag($tag) {
		$page = (int) request()->input('p');
		if($page <= 0) {
			$page = 1;
		}
		//maximum number of posts that can be shown on a page.
		$maxPosts = 20;
		$postsArray = \DB::select('CALL GetPostsHot(?)', array(Tag::getTagIdByName($tag)));
		$posts = [];
		foreach ($postsArray as $p) {
			array_push($posts, new Post((array) $p));
		}
		$posts = collect($posts);
		$posts = $posts->forPage($page, $maxPosts);

		if(auth()->check()) {
			$userUpvotes = auth()->user()->postUpvotes;
			foreach($posts as $post) {
				$post->isUpvoted = false;
				if($userUpvotes->contains('post_id', $post->id)) {
					$post->isUpvoted = true;
				}
			}
		}

		return view('posts/index', ['posts' => $posts, 'tag' => $tag, 'nextPage' => $page+1]);
	}

	/*
	getByTagNew -- fetch all the new posts for a particular tag sorted by time
	*/
	public function getByTagNew($tag) {
		$page = (int) request()->input('p');
		if($page <= 0) {
			$page = 1;
		}
		//maximum number of posts that can be shown on a page.
		$maxPosts = 20;

		$posts = Post::
					where('tag_id', '=', Tag::getTagIdByName($tag))
					->whereDate('created_at', '>=', Carbon::now()->subDays(7))
					->get()->sortByDesc('created_at');

		$posts = $posts->forPage($page, $maxPosts);

		if(auth()->check()) {
			$userUpvotes = auth()->user()->postUpvotes;
			foreach($posts as $post) {
				$post->isUpvoted = false;
				if($userUpvotes->contains('post_id', $post->id)) {
					$post->isUpvoted = true;
				}
			}
		}

		return view('posts/index', ['posts' => $posts, 'tag' => $tag, 'nextPage' => $page+1]);
	}

	/*
	submit() -- return the submit view
	*/
    public function submit() {
		$ipAddress = request()->ip();
		$banned = Banlist::where('ip_address', '=', $ipAddress)->first();
		if(is_null($banned)) {
			return view('posts/submit');
		} else {
			return redirect('/');
		}
    }

	/*
	store() -- POST request for submitting a post
	*/
	public function store() {
		$ipAddress = request()->ip();
		$banned = Banlist::where('ip_address', '=', $ipAddress)->first();
		if(is_null($banned)) {
			$this->validate(request(), [
	            'post-title' => 'required|max:300',
	            'post-body' => 'required|max:40000',
				'post-tag' => ['required', 'regex:/^[a-zA-Z0-9_]+$/', 'max:30'],
				// 'g-recaptcha-response' => 'required|recaptcha'
	        ],
			[
				'post-title.required' => 'Post title cannot be empty.',
				'post-title.max' => 'Post title cannot be more than 300 characters.',
				'post-body.required' => 'Post body cannot be empty.',
				'post-body.max' => 'Post body cannot be more than 40000 characters',
				'post-tag.required' => 'Post tag field cannot be empty.',
				'post-tag.regex' => 'Post tag can only contain alphanumeric characters (A-Z, a-z, 0-9) and underscore (_).',
				'post-tag.max' => 'Post tag cannot be more than 30 characters.',
				'g-recaptcha-response.required' => 'Captcha is required.'
			]
			);
			$tag_name = request('post-tag');
			$post = new Post();
			$post->title = request('post-title');
			$post->body = request('post-body');
			$post->post_type = (int)request('post-type');
			if($post->post_type != 1 && $post->post_type != 2) {
				$post->post_type = 2;
			}
			$post->score = 1;
			$post->report_count = 0;
			$post->ip_address = request()->ip();
			$post->is_deleted = 0;
			if(Tag::getTagIdByName($tag_name) != -2) {
				$post->tag_id = Tag::getTagIdByName($tag_name);
			} else {
				//Tag doesn't exist, create and add to the tags table.
				$new_tag = new Tag();
				$new_tag->name = $tag_name;
				$new_tag->save();
				$post->tag_id = $new_tag->id;
			}
			$post->save();
			return redirect("/tags/$tag_name/$post->id");
		}
		else {
			return redirect('/');
		}
	}

	/*
	getOne -- get a single post using the id
	*/
	public function getOne($tag, $id) {
		$post = Post::find($id);

		if(is_null($post) || $post->is_deleted === 1) {
			abort(404);
		}

		if(auth()->check()) {
			$userUpvotes = auth()->user()->postUpvotes;
			if($userUpvotes->contains('post_id', $post->id)) {
				$post->isUpvoted = true;
			} else {
				$post->isUpvoted = false;
			}
		}

		$comments = Comment::getComments($post->id);
		return view('posts/show-post', ['post' => $post, 'tag' => $tag, 'comments' => $comments]);
	}

	/*
	reportPost -- report a post
	*/
	public function reportPost() {
		$postId = request('postId');
		$post = Post::find($postId);
		if(!is_null($post)) {
			$post->report_count+=1;
			$post->save();
			return json_encode(true);
		}
	}

}
