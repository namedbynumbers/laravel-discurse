<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Banlist;

class CommentController extends Controller
{
	/*
	store -- store a comment
	*/
    public function store($id) {
		$ipAddress = request()->ip();
		$banned = Banlist::where('ip_address', '=', $ipAddress)->first();
		if(is_null($banned)) {
			$this->validate(request(), [
	            'comment-input-body' => 'required',
	        ]);
			$comment = new Comment();
			$comment->post_id = $id;
			if(is_numeric(request('parent-id'))) {
				$comment->parent_id = request('parent-id');
			} else {
				$comment->parent_id = -1;
			}
			$comment->body = request('comment-input-body');
			$comment->score = 1;
			$comment->report_count = 0;
			$comment->ip_address = request()->ip();
			$comment->is_deleted = 0;
			$comment->save();
			return back();
		}
		else {
			return redirect('/');
		}
	}
	
	/*
	reportComment -- report a comment
	*/
	public function reportComment() {
		$commentId = request('commentId');
		$comment = Comment::find($commentId);
		if(!is_null($comment)) {
			$comment->report_count+=1;
			$comment->save();
			return json_encode(true);
		}
	}
}
