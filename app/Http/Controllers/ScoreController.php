<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Post_upvote;
use App\Comment;
use App\Comment_upvote;
use App\Banlist;

class ScoreController extends Controller
{
	public function upvotePost() {
		$postId = request('postId');
		$userId = auth()->user()->id;

		if(!is_null($postId) && !is_null($userId)) {
			$post = Post::find($postId);
			if(count($post)>0) {
				//check if the user has already upvoted the post
				$existingVote = Post_upvote::where('user_id', '=', $userId)->where('post_id', '=', $postId)->get();
				if(count($existingVote) == 0) {
					//increment the score of the post
					$post->score++;
					$post->save();
					//add the user_id and post_id to Post_upvote
					$postUpvote = new Post_upvote();
					$postUpvote->post_id = $postId;
					$postUpvote->user_id = $userId;
					$postUpvote->save();
					return json_encode(true);
				}
			}
		}
	}

	public function upvoteComment() {
		$commentId = request('commentId');
		$userId = auth()->user()->id;

		if(!is_null($commentId) && !is_null($userId)) {
			$comment = Comment::find($commentId);
			if(count($comment)>0) {
				//check if the user has already upvoted the comment
				$existingVote = Comment_upvote::where('user_id', '=', $userId)->where('comment_id', '=', $commentId)->get();
				if(count($existingVote) == 0) {
					//increment the score of the comment
					$comment->score++;
					$comment->save();
					//the add the user_id and comment_id to Comment_upvote
					$commentUpvote = new Comment_upvote();
					$commentUpvote->comment_id = $commentId;
					$commentUpvote->user_id = $userId;
					$commentUpvote->save();
					return json_encode(true);
				}
			}
		}
	}
}
