<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Banlist;

class RegistrationController extends Controller
{
    public function get() {
		$ipAddress = request()->ip();
		$banned = Banlist::where('ip_address', '=', $ipAddress)->first();
		if(is_null($banned)) {
			if(auth()->check()) {
				return redirect('/');
			} else {
				return view('authentication/auth');
			}
		} else {
			return redirect('/');
		}
	}

	public function store() {
		$ipAddress = request()->ip();
		$banned = Banlist::where('ip_address', '=', $ipAddress)->first();
		if(is_null($banned)) {
			if(!auth()->check()) {
				$this->validate(request(),[
					'username' => 'required|max:20|unique:users,username',
					'password' => 'required|min:6'
				]);

				$user = new User();
				$user->username = request('username');
				$user->email = request('email');
				$user->password = bcrypt(request('password'));
				$user->type = 3;	//Regular User
				$user->save();

				auth()->login($user);
			}
		}
		return redirect('/');
	}
}
