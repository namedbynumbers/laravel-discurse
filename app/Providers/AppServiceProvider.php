<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use \App\Banlist;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
	$ipAddress = request()->ip();
	$banned = Banlist::where('ip_address', '=', $ipAddress)->first();
	$isBanned = false;
	if(!is_null($banned)) {
		$isBanned = true;
	}
	View::share('isBanned', $isBanned);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
