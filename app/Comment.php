<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function post() {
		return $this->belongsTo(Post::class, 'post_id');
	}

	public function upvotes() {
		return $this->hasMany(Comment_upvote::class, 'comment_id');
	}

	public static function addReplyList($comments) {
		foreach($comments as $comment) {
			$comment["reply_list"] = [];
		}
		foreach($comments as $comment) {
			if($comment["parent_id"] != -1) {
				if($comments->contains("id", $comment["parent_id"])) {
						$placeholder = $comments[$comment["parent_id"]]["reply_list"];
						array_push($placeholder,  $comment["id"]);
						$comments[$comment["parent_id"]]["reply_list"] = $placeholder;
				}
			}
		}
		return $comments;
	}

	public static function getCommentsByParent($parent) {
		$parent->child_comments = Comment::where('parent_id', '=', $parent->id)->orderBy('score', 'desc')->get();
		$userCommentUpvotes = null;
		if(auth()->check()) {
			$userCommentUpvotes = auth()->user()->commentUpvotes;
		}
		foreach($parent->child_comments as $c) {
			$c->isUpvoted = false;
			if(auth()->check()) {
				if($userCommentUpvotes->contains('comment_id', $c->id)) {
					$c->isUpvoted = true;
				}
			}
			$c->child_comments = [];
			Comment::getCommentsByParent($c);
		}
	}

	public static function getComments($postId) {
		$userCommentUpvotes = null;
		if(auth()->check()) {
			$userCommentUpvotes = auth()->user()->commentUpvotes;
		}
		$comments = Comment::where('post_id', '=', $postId)->where('parent_id', '=', -1)->orderBy('score', 'desc')->get();
		foreach($comments as $c) {
			$c->isUpvoted = false;
			if(auth()->check()) {
				if($userCommentUpvotes->contains('comment_id', $c->id)) {
					$c->isUpvoted = true;
				}
			}
			$c->child_comments = [];
			Comment::getCommentsByParent($c);
		}
		return $comments;
	}

	protected $fillable = array('id', 'post_id', 'parent_id', 'body', 'score', 'created_at', 'updated_at', 'ip_address');
}
