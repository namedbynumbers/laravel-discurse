<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@get');
Route::get('/new', 'PostController@getNew');
Route::get('/submit', 'PostController@submit');
Route::post('/posts/store', 'PostController@store');

Route::get('/posts/{id}', 'PostController@getOne');
Route::post('/posts/{id}/comments/store', 'CommentController@store')->middleware('throttle:5,1');

Route::get('/tags/{tag}', 'PostController@getByTag');
Route::get('/tags/{tag}/new', 'PostController@getByTagNew');
Route::get('/tags/{tag}/{id}', 'PostController@getOne');

Route::get('/auth', 'RegistrationController@get');

Route::post('/register', 'RegistrationController@store')->middleware('throttle:5,1');

Route::post('/login', 'SessionController@store');
Route::get('/logout', 'SessionController@destroy');

Route::post('/upvotePost', 'ScoreController@upvotePost');
Route::post('/upvoteComment', 'ScoreController@upvoteComment');

Route::post('/reportPost', 'PostController@reportPost');
Route::post('/reportComment', 'CommentController@reportComment');

Route::get('/mod', 'AdminController@get');
Route::post('/deletePost', 'AdminController@deletePost');
Route::post('/deleteComment', 'AdminController@deleteComment');

Route::post('/addModerator', 'AdminController@addModerator');
Route::post('/deleteModerator', 'AdminController@deleteModerator');

Route::post('/addBannedIp', 'AdminController@addBannedIp');
Route::post('/deleteBannedIp', 'AdminController@deleteBannedIp');

Route::get('/modlogs', 'AdminController@getModLogs');
Route::post('/addTag', 'AdminController@addTag');
