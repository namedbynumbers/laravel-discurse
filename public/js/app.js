var app = {
	upvotePost: function(id) {
		$.ajax({
			type:'POST',
			url:'/upvotePost',
			headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
			data: {
				postId: id
			},
			success:function(data){
				if(JSON.parse(data) === true) {
					$("#"+id).find(".post-title-upvote").addClass("upvoted-color");
					$("#"+id).find(".post-title-upvote").removeClass("cursor-pointer");
				}
			}
      	});
	},

	upvoteComment: function(id) {
		$.ajax({
			type:'POST',
			url:'/upvoteComment',
			headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
			data: {
				commentId: id
			},
			success:function(data){
				if(JSON.parse(data) === true) {
					$("#comment-"+ id +" > .comment-row-first > .comment-upvote:first-child").addClass('upvoted-color');
					$("#comment-"+ id +" > .comment-row-first > .comment-upvote:first-child").removeClass("cursor-pointer");
				}
			}
      	});
	},

	reportPost: function(id) {
		$.ajax({
			type:'POST',
			url: '/reportPost',
			headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
			data: {
				postId: id
			},
			success: function(data) {
				console.log("Post reported");
				app.showMessage("Post has been reported.");
			}
		});
	},

	reportComment: function(id) {
		$.ajax({
			type: 'POST',
			url: '/reportComment',
			headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
			data: {
				commentId: id
			},
			success: function(data) {
				console.log("Comment reported");
				app.showMessage("Comment has been reported.");
			}
		})
	},

	showMessage: function(data) {
		var messageBoxTextDiv = $('#message-prompt-box > div');
		var messageBox = $('#message-prompt-box');
		messageBoxTextDiv.text(data);
		messageBox.css('display', 'flex');
		messageBox.width($('body').width());
		setTimeout(function() {
			var messageBox = $('#message-prompt-box');
			messageBox.fadeOut(200);
		}, 3000);
	},

	showMessageFail: function(data) {
		var messageBoxTextDiv = $('#message-prompt-box-fail > div');
		var messageBox = $('#message-prompt-box-fail');
		messageBoxTextDiv.text(data);
		messageBox.css('display', 'flex');
		messageBox.width($('body').width());
		setTimeout(function() {
			var messageBox = $('#message-prompt-box-fail');
			messageBox.fadeOut(200);
		}, 3000);
	},

	hideChildComments: function(id) {
		var child_comment_div = $('#comment-'+id+' > .child-comments');
		if(child_comment_div.css('display') === "none") {
			child_comment_div.css('display', 'block');
		} else {
			child_comment_div.css('display', 'none');
		}
	},

	loginToVote: function () {
		app.showMessageFail("Please login to vote.");
	}
}
