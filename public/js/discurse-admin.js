var admin = {
	expandPosts: function(id) {
		var bodyDiv = $('#reported-post-body-'+id);
		if(bodyDiv.css('display') === "none") {
			bodyDiv.css('display', 'block');
		} else {
			bodyDiv.css('display', 'none');
		}
	},
	expandComments: function(id) {
		var bodyDiv = $('#reported-comment-body-'+id);
		if(bodyDiv.css('display') === "none") {
			bodyDiv.css('display', 'block');
		} else {
			bodyDiv.css('display', 'none');
		}
	},
	deletePost: function() {
		var postsToDelete = [];
		$('.reported-post').each(function() {
			if($(this).prop('checked') === true) {
				postsToDelete.push(parseInt($(this).val()));
			}
		})

		if(postsToDelete.length === 0) {
			app.showMessageFail("No posts selected for deletion.");
			return;
		}

		var postDeleteType = $('input[name=postDeleteType]:checked').val();
		var postDeleteComment = $('#postDeleteComment').val();

		$.ajax({
			type: 'POST',
			url: '/deletePost',
			headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
			data: {
				postsToDelete: postsToDelete,
				postDeleteType: postDeleteType,
				postDeleteComment: postDeleteComment
			},
			success: function(data) {
				data = JSON.parse(data);
				if(data === true) {
					location.reload();
				}
				else {
					app.showMessageFail("Post couldn't be deleted. Try again.");
				}
				//app.showMessage("Comment has been reported.");
			},
			error: function(data) {
				app.showMessageFail("Post couldn't be deleted. Try again.");
			}
		})
	},
	deleteComment: function() {
		var commentsToDelete = [];
		$('.reported-comment').each(function() {
			if($(this).prop('checked') === true) {
				commentsToDelete.push(parseInt($(this).val()));
			}
		});

		if(commentsToDelete.length === 0) {
			app.showMessageFail("No comments selected for deletion.");
			return;
		}

		var commentDeleteType = $('input[name=commentDeleteType]:checked').val();
		var commentDeleteComment = $('#commentDeleteComment').val();

		$.ajax({
			type: 'POST',
			url: '/deleteComment',
			headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
			data: {
				commentsToDelete: commentsToDelete,
				commentDeleteType: commentDeleteType,
				commentDeleteComment: commentDeleteComment
			},
			success: function(data) {
				data = JSON.parse(data);
				if(data === true) {
					location.reload();
				}
				else {
					app.showMessageFail("Comment couldn't be deleted. Try again.");
				}
				//app.showMessage("Comment has been reported.");
			},
			error: function(data) {
				app.showMessageFail("Comment couldn't be deleted. Try again.");
			}
		})
	},
	addModerator: function() {
		var username = $('#add-moderator-text').val();
		if(username.length !== 0) {
			$.ajax({
				type: 'POST',
				url: '/addModerator',
				headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
				data: {
					username: username
				},
				success: function(data) {
					data = JSON.parse(data);
					if(data === true) {
						location.reload();
					}
					else {
						app.showMessageFail("Moderator couldn't be added. Try again.");
					}
					//app.showMessage("Comment has been reported.");
				},
				error: function(data) {
					app.showMessageFail("Moderator couldn't be added. Try again.");
				}
			});
		}
	},
	deleteModerator: function(id) {
		$.ajax({
			type: 'POST',
			url: '/deleteModerator',
			headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
			data: {
				userId: id
			},
			success: function(data) {
				data = JSON.parse(data);
				if(data === true) {
					location.reload();
				}
				else {
					app.showMessageFail("Moderator couldn't be removed. Try again.");
				}
				//app.showMessage("Comment has been reported.");
			},
			error: function(data) {
				app.showMessageFail("Moderator couldn't be removed. Try again.");
			}
		});
	},
	addBannedIp: function() {
		var address = $('#add-ip-text').val();
		if(address.length !== 0) {
			$.ajax({
				type: 'POST',
				url: '/addBannedIp',
				headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
				data: {
					address: address
				},
				success: function(data) {
					data = JSON.parse(data);
					if(data === true) {
						location.reload();
					}
					else {
						app.showMessageFail("IP address couldn't be banned. Try again.");
					}
				},
				error: function(data) {
					app.showMessageFail("IP address couldn't be banned. Try again.");
				}
			})
		}
	},
	deleteBannedIp: function(addressId) {
		if(addressId.length !== 0) {
			$.ajax({
				type: 'POST',
				url: '/deleteBannedIp',
				headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
				data: {
					addressId: addressId
				},
				success: function(data) {
					data = JSON.parse(data);
					if(data === true) {
						location.reload();
					}
					else {
						app.showMessageFail("IP address couldn't be banned. Try again.");
					}
				},
				error: function(data) {
					app.showMessageFail("IP address couldn't be banned. Try again.");
				}
			})
		}
	},

	addTag: function() {
		var tagText = $('#add-tag-text').val();
		if(tagText.length !== 0) {
			$.ajax({
				type: 'POST',
				url: '/addTag',
				headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
				data: {
					tagText: tagText
				},
				success: function(data) {
					data = JSON.parse(data);
					if(data === true) {
						location.reload();
					}
					else {
						app.showMessageFail("Tag couldn't be added. Try again.");
					}
					//app.showMessage("Comment has been reported.");
				},
				error: function(data) {
					app.showMessageFail("Tag couldn't be added. Try again.");
				}
			});
		}
	}

}
