var simplemde = null;
$( document ).ready(function() {
	var typeValue = parseInt($('input[name=post-type]:checked').val());
	if(typeValue === 1) {
		var html = $('#post-body-link').html();
		$('#post-body-placeholder').html(html);
	} else if (typeValue === 2) {
		var html = $('#post-body-text').html();
		$('#post-body-placeholder').html(html);
		simplemde = new SimpleMDE({
			forceSync: true,
			element: $("#post-body")[0],
			status:false,
			toolbar: ["bold", "italic", "unordered-list", "ordered-list", "link", "image", "code" ,"quote"]
		});
	}
});

var submitFunctions = {
	changePostType: function(type) {
		if(type === 1) {
			var html = $('#post-body-link').html();
			$('#post-body-placeholder').html(html);
		} else if (type === 2) {
			var html = $('#post-body-text').html();
			$('#post-body-placeholder').html(html);
			simplemde = new SimpleMDE({
				forceSync: true,
				element: $("#post-body")[0],
				status:false,
				toolbar: ["bold", "italic", "unordered-list", "ordered-list", "link", "image", "code", "quote"]
			});
		} else {
			var html = $('#post-body-link').html();
			$('#post-body-placeholder').html(html);
		}
	},

	fillTagInput: function(tagText) {
		$('#post-tag').val(tagText);
	}
}
