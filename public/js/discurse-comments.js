function changeParentComment(id) {
	$('#parent-id').val(id);
	$('#reply-to').text("Replying to Id: "+ id);
}

$( document ).ready(function() {
	var simplemde = null;
	simplemde = new SimpleMDE({
		forceSync: true,
		spellChecker: false,
		element: $("#comment-input-body")[0],
		status:false,
		toolbar: ["bold", "italic", "unordered-list", "ordered-list", "link", "image", "code", "quote"]
	});
});
