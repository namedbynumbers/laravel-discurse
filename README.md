#discurse
A simple discussion forum. Or an illegitimate child of reddit and 4chan.

###Features/Peculiarities
* Threads and comments can be posted anonymously.
* Posts can be categorized based on tags.
* Posts and comments can be upvoted, using a registered account. The registered account only serves to track the posts/comments you have upvoted. The posts/comments posted while logged-in are not tied to your account, they are still anonymous.
* Only the last 7 days of posts are indexed on the frontpage. Posts older than 7 days can still be commented on, but they aren't accessible from the frontpage. (Might be disabled in the future.)

###Self-hosting
Probably not a good idea right now. A good part of the functionality is unfinished. Vital parts of the code, like the SQL stored procedures are not even present in the repo. You'll have to wait for me till I figure out how to store them in migrations.

[DEMO](https://www.discurse.xyz)
