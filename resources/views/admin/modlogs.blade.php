@extends('layouts.layout')
@section('includes')
  <link href="{{ asset('css/discurse-mod.css') }}" rel="stylesheet">
@endsection

@section('script-includes')
  <script src="{{ asset('js/discurse-admin.js') }}"></script>
@endsection

@section('content')
	Note: Details of posts/comments with personal info are not visible. <br />
	<strong>Deleted Posts</strong> <br />
		<div class="indented-div">
			@if(count($deletedPosts) != 0)
				@foreach ($deletedPosts as $deletedPost)
					<div class="deleted-post-div">
						<span onclick="admin.expandPosts({{$deletedPost->post_id}})" class="reported-comments-expand-btn cursor-pointer">► ID: {{$deletedPost->post_id}} </span> |
						Reason:
						@if($deletedPost->delete_type == 1)
							Spam
						@elseif($deletedPost->delete_type == 2)
							Personal Info
						@elseif($deletedPost->delete_type == 3)
							Other
						@endif
						| Deleted on : {{ $deletedPost->created_at->toDayDateTimeString()}}
						<br />
						Mod Comments:
						{{$deletedPost->delete_reason}}
						<br />
						@if($deletedPost->delete_type != 2)
						<div class="reported-post-body" id="reported-post-body-{{$deletedPost->post_id}}">
							{{$deletedPost->post->body}}
						</div>
						@endif
					</div>
				@endforeach
			@else
				No reported posts.
			@endif
		</div>

	<strong>Deleted Comments</strong> <br />
		<div class="indented-div">
			@if(count($deletedComments) != 0)
				@foreach ($deletedComments as $deletedComment)
					<div class="deleted-post-div">
						<span onclick="admin.expandComments({{$deletedComment->comment_id}})" class="reported-comments-expand-btn cursor-pointer">► ID: {{$deletedComment->comment_id}} </span> |
						<a href="/tags/{{$deletedComment->comment->post->tag->name}}/{{$deletedComment->comment->post->id}}">Post</a> <br />
						Reason:
						@if($deletedComment->delete_type == 1)
							Spam
						@elseif($deletedComment->delete_type == 2)
							Personal Info
						@elseif($deletedComment->delete_type == 3)
							Other
						@endif
						<br />
						Mod Comments:
						{{$deletedComment->delete_reason}}
						<br />
						@if($deletedComment->delete_type != 2)
						<div class="reported-comment-body" id="reported-comment-body-{{$deletedComment->comment_id}}">
							{{$deletedComment->comment->body}}
						</div>
						@endif
					</div>
				@endforeach
			@else
				No reported posts.
			@endif

		</div>
@endsection
