@extends('layouts.layout')
@section('includes')
  <link href="{{ asset('css/discurse-mod.css') }}" rel="stylesheet">
@endsection

@section('script-includes')
  <script src="{{ asset('js/discurse-admin.js') }}"></script>
@endsection

@section('content')
	<strong>Admin/Mod Panel</strong> <br />

	@if(auth()->user()->type == 1)
		<hr />
		<strong>Admin </strong> <br />
		<span>Add a Tag</span> <br />
		<div class="indented-div">
			<input type="text" id="add-tag-text" name="add-tag" />
			<span class="cursor-pointer add-btn" onclick="admin.addTag()"> Add </span>
		</div>
		{{-- Add Moderator --- START --}}
		<span>Add a moderator</span> <br />
		<div class="indented-div">
			<input type="text" id="add-moderator-text" name="add-moderator" />
			<span class="cursor-pointer add-btn" onclick="admin.addModerator()"> Add </span>
		</div>
		<span>List of moderators</span> <br />
		<div class="indented-div">
			@foreach($moderators as $moderator)
				<div>
					{{ $moderator->username }} <span class="cursor-pointer delete-btn" onclick="admin.deleteModerator({{$moderator->id}})"> Delete </span>
				</div>
			@endforeach
		</div>
		{{-- Add Moderator --- END --}}

		{{-- Ban IP --- START --}}
		<span>Ban IP</span> <br />
		<div class="indented-div">
			<input type="text" id="add-ip-text" name="add-ip" />
			<span class="cursor-pointer add-btn" onclick="admin.addBannedIp()"> Add </span> <br />
		</div>
		<span>Banned IP's</span> <br />
		<div class="indented-div">
			@foreach($banlist as $address)
				<div>
					{{ $address->ip_address }} <span class="cursor-pointer delete-btn" onclick="admin.deleteBannedIp({{$address->id}})"> Delete </span>
				</div>
			@endforeach
		</div>
		{{-- Ban IP --- END --}}
	@endif
	<hr />
	<strong>Moderator</strong> <br />

	{{-- Reported Posts --- START --}}
	<span>Reported Posts</span> <br />
	<div class="indented-div">
		@if(count($posts) != 0)
			@foreach ($posts as $post)
				<input type="checkbox"  id={{"post-id-".$post->id}} class="reported-post" value="{{$post->id}}"/><span onclick="admin.expandPosts({{$post->id}})" class="reported-posts-expand-btn cursor-pointer">► ({{$post->report_count}} reports) ID: {{$post->id}} | {{"@".$post->tag->name}} | {{ $post->title}} </span> |
				{{$post->ip_address}} <br />
				<div class="reported-post-body" id="reported-post-body-{{$post->id}}">
					{{$post->body}}
				</div>
			@endforeach
		@else
			No reported posts.
		@endif

		<div id="delete-posts-div">
			Delete Posts: <br />
			Reason:
			<input type="radio" id="postDeleteType" name="postDeleteType" value="1" /> Spam &nbsp;&nbsp;&nbsp;
			<input type="radio" id="postDeleteType" name="postDeleteType" value="2" /> Personal Info &nbsp;&nbsp;&nbsp;
			<input type="radio" id="postDeleteType" name="postDeleteType" value="3" /> Other &nbsp;&nbsp;&nbsp; <br />

			Comment:
			<input type="text" id="postDeleteComment" /> <br />
			<span class="delete-btn cursor-pointer" onclick="admin.deletePost()">Delete</span>

		</div>
	</div>
	{{-- Reported Posts --- END --}}

	{{-- Reported Comments --- START --}}
	<span>Reported Comments</span>
	<div class="indented-div">
		@if(count($comments) != 0)
			@foreach ($comments as $comment)
				<input type="checkbox"  id={{"comment-id-".$comment->id}} class="reported-comment" value="{{$comment->id}}"/>
				<span onclick="admin.expandComments({{$comment->id}})" class="reported-comments-expand-btn cursor-pointer">► ({{$comment->report_count}} reports) ID: {{$comment->id}} </span> |
				<a href="/tags/{{$comment->post->tag->name}}/{{$comment->post->id}}">Post</a> |
				{{$comment->ip_address}} <br />
				<div class="reported-comment-body" id="reported-comment-body-{{$comment->id}}">
					{{$comment->body}}
				</div>
			@endforeach
		@else
			No reported comments.
		@endif

		<div id="delete-posts-div">
			Delete Comments: <br />
			Reason:
			<input type="radio" id="commentDeleteType" name="commentDeleteType" value="1" /> Spam &nbsp;&nbsp;&nbsp;
			<input type="radio" id="commentDeleteType" name="commentDeleteType" value="2" /> Personal Info &nbsp;&nbsp;&nbsp;
			<input type="radio" id="commentDeleteType" name="commentDeleteType" value="3" /> Other &nbsp;&nbsp;&nbsp; <br />

			Comment:
			<input type="text" id="commentDeleteComment" /> <br />
			<span class="delete-btn cursor-pointer" onclick="admin.deleteComment()">Delete</span>

		</div>
	</div>
	{{-- Reported Comments --- END --}}
	{{ csrf_field() }}
@endsection
