@extends('layouts.layout')

@section('includes')
  <link href="{{ asset('css/discurse-auth.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div id="auth-div">
		<div id="login-div">
			Login
			<br />
			<br />
			<form method="POST" action="/login">
				{{ csrf_field() }}
				<div class="post-row">
		            <label for="username" class="post-label">Username: </label>
		            <input type="text" class="post-input" id="username" name="username" required><br>
		        </div>

				<div class="post-row">
		            <label for="password" class="post-label">Password: </label>
		            <input type="password" class="post-input" id="password" name="password" required><br>
		        </div>

				<div class="post-row">
		            <input type="submit" id="login" name="login" value="Login"><br>
		        </div>
			</form>
		</div>
		<div id="registration-div">
			Register
			<br />
			<br />
			<form method="POST" action="/register">
				{{ csrf_field() }}
				<div class="post-row">
		            <label for="username" class="post-label">Username: </label>
		            <input type="text" class="post-input" id="username" name="username" required><br>
		        </div>

				<div class="post-row">
					<label for="email" class="post-label">Email:</label>
					<input type="email" class="post-input" id="email" name="email" /><br />
				</div>

				<div class="post-row">
		            <label for="password" class="post-label">Password: </label>
		            <input type="password" class="post-input" id="password" name="password" required><br>
		        </div>

				<div class="post-row">
		            <input type="submit" id="login" name="register" value="Register"><br>
		        </div>
			</form>
		</div>
	</div>
	<div class="post-row">
		<label class="post-label"></label>
		@include('layouts/errors')
	</div>
@endsection
