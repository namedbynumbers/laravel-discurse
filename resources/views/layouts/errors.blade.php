@if(count($errors))
	<ul>
			@foreach($errors->all() as $error)
				<li class="error-text">{{ $error }}</li>
			@endforeach
	</ul>
@endif
