<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>disc#rse</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="{{ asset('css/discurse-main.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="icon" type="image/png" href="{{ asset('img/favicon.png')}}" >
	<script src='https://www.google.com/recaptcha/api.js'></script>
	@yield('includes')
</head>
<body>
	<div id="app">
		@include('layouts.navbar')
		<hr class="hr-bottom">
		<div id="content">
      		@yield('content')
		</div>
		<hr/>
		<div id="message-prompt-box">
			<div>
				Your message goes here.
			</div>
		</div>
		<div id="message-prompt-box-fail">
			<div>
				Your message goes here.
			</div>
		</div>
		@include('layouts.footer')
	</div>
	<!--script includes BEGIN-->
	{{-- <script src="./js/vue.js"></script>
  	<script src="./js/app.js"></script> --}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="{{ asset('js/app.js') }}"></script>
	@yield('script-includes')
  	<!--script includes END-->
</body>
</html>
