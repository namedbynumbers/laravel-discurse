<div class="comment-row" id="comment-{{$comment->id}}">
	<div class="comment-row-first">
		@if(auth()->check())
			@if($comment->is_deleted == 0)
				@if(!$comment->isUpvoted)
					<div class="comment-upvote upvote-col cursor-pointer" onclick="app.upvoteComment({{$comment->id}})">▲</div>
				@else
					<div class="comment-upvote upvote-col upvoted-color">▲</div>
				@endif
			@else
				<div class="comment-upvote upvote-col deleted-comment">▲</div>
			@endif
		@else
			<div class="comment-upvote upvote-col cursor-pointer" onclick="app.loginToVote()">(▲) </div>
		@endif
		<div class="comment-details-col">
			@if($comment->is_deleted == 0)
			<div class="comment-details">
				ID: {{$comment->id}} |
				<span class="cursor-pointer" onclick="app.reportComment({{$comment->id}})">report</span> |
				<a href="#comment-form" class="reply-button" onclick="changeParentComment({{$comment->id}})">Reply</a> |
				<span class="cursor-pointer" onclick="app.hideChildComments({{$comment->id}})">[ - ]</span>
			</div>
			<div class="comment-body" id="comment-body-{{$comment->id}}">
				{!! Markdown::convertToHtml($comment->body) !!}
			</div>
			@else
				<div class="deleted-comment">
					[Deleted]
				</div>
			@endif

		</div>
	</div>
	<div class="child-comments">
		@if($comment->child_comments->count() != 0)
			@foreach($comment->child_comments as $comment)
				@include('layouts.comment', ['comment' => $comment])
			@endforeach
		@endif
	</div>
</div>
