<div id="pinned-tags-div">
	<div id="pinned-tag-label">Pinned Tags — </div>
	<div class="pinned-tag-btn no-border-left-padding"><a href="/tags/random">random</a></div>
	<div class="pinned-tag-btn"><a href="/tags/games">games</a></div>
	<div class="pinned-tag-btn"><a href="/tags/movies">movies</a></div>
	<div class="pinned-tag-btn"><a href="/tags/music">music</a></div>
	<div class="pinned-tag-btn"><a href="/tags/television">television</a></div>
	<div class="pinned-tag-btn"><a href="/tags/programming">programming</a></div>
	<div class="pinned-tag-btn"><a href="/tags/anime">anime</a></div>
	<div class="pinned-tag-btn"><a href="/tags/technology">technology</a></div>
	<div class="pinned-tag-btn"><a href="/tags/literature">literature</a></div>
	<div class="pinned-tag-btn"><a href="/tags/serious">serious</a></div>
	<div class="pinned-tag-btn pinned-tags-filler" ></div>
</div>
<div id="header">
  <div id="title">
		<div id="logo">
		  <img src="{{asset('img/logo.png')}}" />
		</div>
		<a href='/' id="title-text">discurse</a>
  </div>
</div>
<div id="navbar">
@if(@isset($tag))
	<div id="navbar-left">
		<div id="header-tag-name">
			<a class="nav-button no-left-border" href="#">{{"@". $tag}}</a>
			<div id="arrow-right">
				<img src="{{asset('img/arrow.png')}}" />
			</div>
		</div>
		<div id="navbar-buttons-div">
			<a class="nav-button" href="/tags/{{$tag}}/">popular</a>
			<a class="nav-button" href="/tags/{{$tag}}/new">new</a>

			@if(!$isBanned)
				<a class="nav-button" href="/submit">submit</a>
			@endif

			<a class="nav-button" href="/modlogs">modlogs</a>
			@if(auth()->check() && (auth()->user()->type == 1 || auth()->user()->type == 2))
				<a class="nav-button" href="/mod">mod</a>
			@endif
		</div>
	</div>
	<div id="navbar-right">
		@if(auth()->check())
			<a class="nav-button" href="#">{{ auth()->user()->username }}</a>
			<a class="nav-button no-left-border" href="/logout"> (Logout) </a>
		@else
			@if(!$isBanned)
				<a class="nav-button no-left-border" class="nav-link" href="/auth"> login/register  </a>
			@endif
		@endif
	</div>
@else
	<div id="navbar-left">
	<div id="header-tag-name">
		<a class="nav-button no-left-border" href="#">@frontpage</a>
		<div id="arrow-right">
			<img src="{{asset('img/arrow.png')}}" />
		</div>
	</div>
	<div id="navbar-buttons-div">
		<a class="nav-button" href="/">popular</a>
		<a class="nav-button" href="/new">new</a>

		@if(!$isBanned)
			<a class="nav-button" href="/submit">submit</a>
		@endif

		<a class="nav-button" href="/modlogs">modlogs</a>
		@if(auth()->check() && (auth()->user()->type == 1 || auth()->user()->type == 2))
			<a class="nav-button" href="/mod">mod</a>
		@endif
		</div>
	</div>
	<div id="navbar-right">
		@if(auth()->check())
			<a class="nav-button" href="#">{{ auth()->user()->username }}</a>
			<a class="nav-button no-left-border" href="/logout"> (Logout) </a>
		@else
			@if(!$isBanned)
				<a class="nav-button no-left-border" class="nav-link" href="/auth"> login/register  </a>
			@endif
		@endif
	</div>
@endif
</div>
