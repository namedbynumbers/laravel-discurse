@extends('layouts.layout')

@section('includes')
  <link href="{{ asset('css/discurse-index.css') }}" rel="stylesheet">
@endsection

@section('content')
	@if(count($posts) != 0)
		@foreach($posts as $post)
			<div class="post-div" id="{{$post->id}}">
			<div class="post-title">
				@if(auth()->check())
					@if(!$post->isUpvoted)
						<div class="post-title-upvote cursor-pointer" onclick="app.upvotePost({{$post->id}})">(▲) </div>
					@else
						<div class="post-title-upvote upvoted-color">(▲) </div>
					@endif
				@else
					<div class="post-title-upvote cursor-pointer" onclick="app.loginToVote()">(▲) </div>
				@endif

				@if($post->post_type == 1)
					@if(starts_with($post->body, 'http'))
						<a class="post-title-text" href="{{ $post->body }}">{{ $post->title }}</a>
					@else
						<a class="post-title-text" href="{{ '//'.$post->body }}">{{ $post->title }}</a>
					@endif
				@elseif($post->post_type == 2)
					<a class="post-title-text" href="/tags/{{$post->tag->name}}/{{$post->id}}">{{ $post->title }}</a>
				@endif
			</div>
			<div class="post-details">
				<a href="/tags/{{$post->tag->name}}">{{"@".$post->tag->name}}</a> |
				<a href="/tags/{{$post->tag->name}}/{{$post->id}}"> {{count($post->comments)}} comments </a> |
				{{$post->created_at->diffForHumans()}} |
				<span class="cursor-pointer" onclick="app.reportPost({{$post->id}})">report</span>
			</div>
			</div>
		@endforeach
		<a href="{{Request::url()}}?p={{$nextPage}}">More ►</a>
	@else
		No more posts to show :(
	@endif
	{{ csrf_field() }}
@endsection
