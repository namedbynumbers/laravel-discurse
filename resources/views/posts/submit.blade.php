@extends('layouts.layout')
@section('includes')
  <link href="{{ asset('css/discurse-submit.css') }}" rel="stylesheet">
  <link href="{{ asset('css/discurse-errors.css') }}" rel="stylesheet">
  <link href="{{ asset('css/simplemde.min.css') }}" rel="stylesheet">
@endsection
@section('script-includes')
  <script src="{{ asset('js/discurse-submit.js') }}"></script>
  <script src="{{ asset('js/simplemde.min.js')}}"></script>
@endsection

@section('content')
    <p>Submit to disc#rse</p>
    <form id="submit-post-form" method="POST" action="/posts/store">
		{{ csrf_field() }}
		<div class="post-row">
			<label for="type" class="post-label">Post Type</label>
			<div>
				<input type="radio"  id="post-type" name="post-type" value="1" checked onclick="submitFunctions.changePostType(1)"/> Link &nbsp;&nbsp;&nbsp;
				<input type="radio"  id="post-type" name="post-type" value="2" onclick="submitFunctions.changePostType(2)"/> Text
			</div>
		</div>
        <div class="post-row">
            <label for="title" class="post-label">Title</label>
            <input type="text" class="post-input" id="post-title" name="post-title" required><br>
        </div>

		<div class="post-row">
	        <label for="tag" class="post-label">Tag</label>
	        <input type="text" class="post-input" id="post-tag" name="post-tag" required> <br>
		</div>

		<div class="post-row">
	        <label for="tag" class="post-label"></label>
	        <span class="post-input small-text">
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('random')">random </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('games')">games </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('movies')">movies </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('music')">music </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('television')">television </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('programming')">programming </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('anime')">anime </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('technology')">technology </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('literature')">literature </span>
				<span class="cursor-pointer" onclick="submitFunctions.fillTagInput('serious')">serious </span>
			</span>
		</div>

		<div class="post-row" id="post-body-placeholder">

		</div>

		<div class="post-row">
	        <label for="tag" class="post-label">Captcha: </label>
	        {!! Recaptcha::render() !!}
		</div>

		<div class="post-row">
			<label class="post-label"></label>
	        <input type="submit" id="submitBtn" name="submitBtn" value="Submit">
		</div>
		<div class="post-row">
			<label class="post-label"></label>
	        @include('layouts/errors')
		</div>
	</form>
	<script type="template" id="post-body-link">
		<label for="body" class="post-label">Link</label>
		<input type="text" class="post-input" id="post-body" name="post-body" required><br>
	</script>
	<script type="template" id="post-body-text">
		<label for="body" class="post-label">Text</label>
		<div id="post-body-mde">
		<textarea id="post-body" class="post-input text-area-height" name="post-body"></textarea><br>
		</div>
	</script>
@endsection
