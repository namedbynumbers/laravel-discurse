@extends('layouts.layout')
@section('includes')
	<link href="{{ asset('css/discurse-show-post.css') }}" rel="stylesheet">
	<link href="{{ asset('css/simplemde.min.css') }}" rel="stylesheet">
@endsection

@section('script-includes')
	<script src="{{ asset('js/discurse-comments.js') }}"></script>
	<script src="{{ asset('js/simplemde.min.js')}}"></script>
@endsection

@section('content')
	{{-- POST BODY BEGIN --}}
	<div class="post-title-row" id="{{ $post->id }}">
		@if(auth()->check())
			@if(!$post->isUpvoted)
				<div class="post-title-upvote cursor-pointer" onclick="app.upvotePost({{$post->id}})">(▲)</div>
			@else
				<div class="post-title-upvote upvoted-color">(▲)</div>
			@endif
		@else
			<div class="post-title-upvote cursor-pointer" onclick="app.loginToVote()">(▲) </div>
		@endif

		@if($post->post_type == 1)
			@if(starts_with($post->body, 'http'))
				<div id="post-title-text"><a href="{{ $post->body }}"><strong class="post-title-color">{{ $post->title }}</strong></a></div>
			@else
				<div id="post-title-text"><a href="{{ '//'. $post->body }}"><strong class="post-title-color">{{ $post->title }}</strong></a></div>
			@endif

		@elseif($post->post_type == 2)
			<div id="post-title-text"><strong>{{$post->title}}</strong></div>
		@endif
	</div>

	<div id="post-body-row">
		<div id="left-offset"></div>
		<div id="post-body-content">
			@if($post->post_type == 2)
				{!! Markdown::convertToHtml($post->body) !!}
			@endif
		</div>
	</div>

	<div id="post-details-row">
		{{"@".$post->tag->name}} |
		{{ count($comments) }} comments |
		{{$post->created_at->diffForHumans()}} |
		<span class="cursor-pointer" onclick="app.reportPost({{$post->id}})">report</span>
	</div>
	<hr>
	{{-- POST BODY END --}}

	{{-- COMMENT INPUT BEGIN --}}
	@if(!$isBanned)
	    <form method="POST" action="/posts/{{$post->id}}/comments/store" id="comment-form">
			{{ csrf_field() }}
			<div id="reply-to"></div>
			<input type="hidden" name="parent-id" id="parent-id" value="-1" />
			<div class="comment-input-row">
				<div>
				<textarea id="comment-input-body" class="comment-input-input" name="comment-input-body" placeholder="Submit a comment" required></textarea><br>
				</div>
			</div>

			<div class="comment-input-row">
		        <input type="submit" id="submitBtn" name="submitBtn" value="Submit">
			</div>
			<div class="comment-input-row">
		        @include('layouts/errors')
			</div>
		</form>
	@endif
	{{-- COMMENT INPUT END --}}

	{{-- ALL COMMENTS BEGIN--}}
	<div id="comments-section">

		@foreach($comments as $comment)
			@include('layouts.comment', ['comment' => $comment])
		@endforeach
	</div>
	{{-- ALL COMMENTS END --}}

	{{-- REPLY HOVER BOX  --}}
	<div id="hover-reply-box">

	</div>
	{{ csrf_field() }}
@endsection
